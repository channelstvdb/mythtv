#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2014 Jonas Fourquier <jonas@mythtv-fr.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

__title__ ="ChannelTVBD-MythTV";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="dev"
__usage__ ='''
%(title)s
Version: %(version)s
Author: %(author)s

Introduce xmltvid, logo, real channel name in MythTV with ChannelTVBD
service.

Usage:
%(file)s -h
%(file)s -c ...
%(file)s -g ... [-p ...] [-s ...] [--replace-names] [--replace-icons] [--replace-xmltvids]
  -h, --help        show this help message and exit
  -c, --country     display a list for available grabbers for country.
  -g, --grabberid   grabber_id for search in ChannelTVBD service.
  -p, --path        logos directory path (/var/mythtv/channels by
                    default).
  -s, --sourceid    Update xmltvid only for this sourceid (by default
                    search xmltvid for all sources.
  --replace-names   Replace existing channels names (by default no
                    replace if a name exist)
  --replace-icons   Replace existing icons (by default no replace if an
                    icon exist)
  --replace-xmltvids Replace existing xmltvids (by default no replace if
                    a xmltvids exist)
'''%{'title':__title__, 'file':__file__, 'version':__version__,'author':__author__}

import os, sys, requests, logging
from getopt import getopt
from MythTV import MythDB
from pprint import pprint
logging.basicConfig(level=logging.INFO,format='%(levelname)s:  %(message)s')
logger = logging.getLogger(__title__)
API_URL = 'http://channelstvdb.mythtv-fr.org/api/';

opts,args = getopt(sys.argv[1:], "hc:g:rp:s:",["help","country=","grabber_id=","path","sourceid=","replace-names","replace-icons","replace-xmltvids"])
countrys = False
grabberid = False
replace_names = False
replace_icons = False
replace_xmltvids = False
path = '/var/mythtv/channels'
sourceid = False
for opt, arg in opts:
    if opt in ("-h", "--help"):
        sys.stdout.write(__usage__)
        sys.exit(0)
    elif opt in ("-c", "--countrys"):
        countrys = arg
    elif opt in ("-g", "--grabberid"):
        grabberid = arg
    elif opt in ("--replace-names"):
        replace_names = True
    elif opt in ("--replace-icons"):
        replace_icons = True
    elif opt in ("--replace-xmltvids"):
        replace_xmltvids = True
    elif opt in ("-p", "--path"):
        path = arg
    elif opt in ("-s", "--sourceid"):
        if arg.isdigit() :
            sourceid = arg
        else :
            sys.stdout.write("Error: sourceid must be a numeric.\n")
            sys.exit(1)

if (not countrys and not grabberid) or (countrys and grabberid):
    sys.stdout.write("Syntax error\n--help for more information\n")
    sys.exit(1)

if countrys:
    r = requests.post(API_URL+'get_grabbers',params = {'countrys':countrys})
    print r.url
    if len(r.json()['data']['grabbers']) == 0 :
        sys.stdout.write("grabbers no found\n")
    for g in r.json()['data']['grabbers'] :
        sys.stdout.write("{0:25} {1:1} -g {2:1} [-r] [-p ...] [-s ...]\n".format(g['name'], __file__, g['grabber_id']))
    sys.exit(0)

whereClause = ''
if sourceid :
    whereClause += ' AND sourceid = '+sourceid

logger.info('MythDB: connect')
cr = MythDB().cursor()
cr.execute('SELECT chanid, name, xmltvid, icon FROM channel WHERE 1' + whereClause)
for chanid,oldName, oldXmltvid, oldIcon in cr.fetchall() :
    if oldName and (replace_names or replace_xmltvids or replace_icons or not oldName or not oldIcon or not oldXmltvid) :
        logger.info('ChannelTVBD: get_channel %s (chanid %s)'%(oldName,chanid))
        r = requests.post(API_URL+'get_channel',params = {"alias":oldName,"grabber_id":grabberid})
        newchan = r.json()['data']['channel']
        if not (newchan) :
            # TODO utilisez api/search_logos puis envoyer la proposition
            logger.info('   => no found. Please add channel for this alias on %s/../alias/unknow'%API_URL)
        else :
            logger.info('   => found')
            sql_set = []
            newchan['locallogo'] = ''
            if replace_names :
                sql_set.append('name="%(name)s"'%newchan)
            if replace_xmltvids :
                sql_set.append('xmltvid="%(xmltvid)s"'%newchan)
            if newchan['logo'] and (not oldIcon or replace_icons) :
                r_logo = requests.get(newchan['logo'])
                if r_logo.status_code == 200:
                    print path,newchan['channel_id'],os.path.splitext(r_logo.url)[1]
                    newchan['locallogo'] = os.path.join(path,newchan['channel_id']+os.path.splitext(r_logo.url)[1])
                    with open(newchan['locallogo'], 'wb') as f:
                        for chunk in r_logo.iter_content(1024):
                            f.write(chunk)
                    sql_set.append('icon="%(locallogo)s"'%newchan)
            if len(sql_set) > 0 :
                query = 'UPDATE channel SET %s WHERE chanid=%i'%(','.join(sql_set),chanid)
                print (query)
                cr.execute(query)

sys.exit(0)
